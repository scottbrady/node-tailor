var Tailor  = require('../..'),
    tailor  = new Tailor();

tailor.configure({
    path : __dirname + '/lib/controller',
    port   : 8080
});

tailor.listen();