var async   = require('async'),
    _       = require('underscore'),
    Express = require('express'),
    fs      = require('fs');

function Tailor() {
    this.express = Express();
    this.express.use(this.express.router);
}

_.extend(Tailor.prototype, {

    //////////////////////////////////////////////////////////////////////////
    // Public methods ///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    configure : function(options) {
        this.options = options;
    },

    listen : function() {
        var self = this;

        async.series([
            function(callback) {
                self._findControllers(self.options.path, function(controllers) {
                    self._controllers = controllers;
console.log(self._controllers)
                });

                callback();
            },

            function(callback) {
                self._registerRoutes();

                self.express.listen(self.options.port, function() {
                    console.log('Tailor HTTP server listening on port ' + self.options.port + '.');
                });

                callback();
            }
        ]);
    },

    //////////////////////////////////////////////////////////////////////////
    // Psuedo-private methods ///////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    _findControllers : function(path, callback) {
        var self        = this,
            controllers = [];

        fs.readdir(path, function(error, file) {

            if (file.indexOf('.') === 0) {
                return;
            }

            fullPath = path + '/' + file;

            fs.stat(fullPath, function(error, stat) {
                if (stat && stat.isDirectory()) {
                    self._findControllers(fullPath, function(_controllers) {
                        controllers = _.union(controllers, _controllers);
                        callback(controllers);
                    });
                } else {
                    controllers.push(
                        self._normalizePath(path, fullPath)
                    );
                    callback(controllers);
                }
            });
        });
    },

    _handleRequest : function(request, response) {
        if (_.indexOf(this._controllers, request.url) >= 0) {
console.log(request.url)
        }
    },

    _normalizePath : function(base, path) {
        return path.replace(base, '')
            .replace('.js', '');
    },

    _registerRoutes : function() {
        this.express.all('*', _.bind(this._handleRequest, this));
    }
});

module.exports = Tailor;